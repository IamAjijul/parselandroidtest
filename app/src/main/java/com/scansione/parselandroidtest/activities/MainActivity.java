package com.scansione.parselandroidtest.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.scansione.parselandroidtest.R;
import com.scansione.parselandroidtest.dialogs.Alerts;
import com.scansione.parselandroidtest.fragment.MapDialogFragment;
import com.scansione.parselandroidtest.helper.AppUtils;
import com.scansione.parselandroidtest.helper.MarshmallowPermissionHelper;
import com.scansione.parselandroidtest.interfaces.Constant;
import com.scansione.parselandroidtest.interfaces.PaperdbKeys;
import com.scansione.parselandroidtest.network.BaseResponseListener;
import com.scansione.parselandroidtest.network.RequestClient;
import com.scansione.parselandroidtest.services.LocationFetcherService;

import org.json.JSONException;
import org.json.JSONObject;

import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity {
    private static final String LOG = "LOGIN ACTIVITY";
    private EditText login_etEmail, login_etPass;
    private String strEmail, strPass;
    private CoordinatorLayout login_clParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        initClassViews();

    }

    private void initClassViews() {
        Log.d(LOG, "FCM TOKEN" + Paper.book().read(PaperdbKeys.FCM_TOKEN, ""));
        login_etEmail = (EditText) findViewById(R.id.login_etEmail);
        login_etPass = (EditText) findViewById(R.id.login_etPass);
        login_clParent = (CoordinatorLayout) findViewById(R.id.login_clParent);
    }

    public void onSubmitClick(View view) {
        if (isValidationPass()) {
            if (MarshmallowPermissionHelper.checkDeviceLocationSetting(this, new Alerts.TwoButtonAlert() {
                @Override
                public void onPositiveButtonClick(DialogInterface dialogInterface) {
                    dialogInterface.dismiss();
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    myIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivityForResult(myIntent, Constant.LOCATION_SETTING_REQUEST_CODE);
                }

                @Override
                public void onNegativeButtonClick(DialogInterface dialogInterface) {
                    dialogInterface.dismiss();
                }
            })) {
                tryLogin();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.LOCATION_SETTING_REQUEST_CODE) {

            tryLogin();


        }
    }

    private void tryLogin() {
        new RequestClient(this).callLoginApi(this, strEmail, strPass, "Android",
                Paper.book().read(PaperdbKeys.FCM_TOKEN, ""), "12345", "Signing In...", new BaseResponseListener() {
                    @Override
                    public void requestStarted() {

                    }

                    @Override
                    public void requestCompleted(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("responseCode").equalsIgnoreCase("1000")) {
                                Snackbar.make(login_clParent, getResources().getString(R.string.success), Snackbar.LENGTH_SHORT).addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                                    @Override
                                    public void onDismissed(Snackbar transientBottomBar, int event) {
                                        super.onDismissed(transientBottomBar, event);
                                        if (MarshmallowPermissionHelper.getLocationPermission(null, MainActivity.this,
                                                Constant.LOCATION_REQUEST_CODE)) {
                                            showMapAndFetchLocation();

                                        }
                                    }
                                }).show();

                            } else {
                                generateError(getResources().getString(R.string.error));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void requestEndedWithError(VolleyError error) {
                        generateError(getResources().getString(R.string.error));
                    }
                });
    }

    private boolean isValidationPass() {
        gatherAllInputData();
        if (strEmail.equalsIgnoreCase("")) {
            generateError("Please enter email-id");
            return false;
        } else if (!AppUtils.isValidEmail(strEmail)) {
            generateError("Please enter valid email-id");
            return false;
        } else if (strPass.equalsIgnoreCase("")) {
            generateError("Please enter password");
            return false;
        }

        return true;
    }

    private void generateError(String s) {
        Snackbar.make(login_clParent, s, Snackbar.LENGTH_SHORT).show();
    }

    private void gatherAllInputData() {
        strEmail = login_etEmail.getText().toString();
        strPass = login_etPass.getText().toString();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showMapAndFetchLocation();
                }
                break;
        }
    }

    private void showMapAndFetchLocation() {
        startService(new Intent(MainActivity.this, LocationFetcherService.class));
        MapDialogFragment mapDialogFragment = new MapDialogFragment();
        mapDialogFragment.show(getSupportFragmentManager(), "MapDialog");
    }
}
