package com.scansione.parselandroidtest.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.scansione.parselandroidtest.interfaces.Constant;


public class LocationReceiver extends BroadcastReceiver {
    private LocationListener locationListener;

    public LocationReceiver(LocationListener locationListener) {
        this.locationListener = locationListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && locationListener != null) {
            locationListener.onLicationChange(intent.getDoubleExtra(Constant.LATITUDE, 0.00)
                    , intent.getDoubleExtra(Constant.LONGITUDE, 0.00));
        }
    }

    public interface LocationListener {
        void onLicationChange(Double lat, Double lng2);
    }
}
