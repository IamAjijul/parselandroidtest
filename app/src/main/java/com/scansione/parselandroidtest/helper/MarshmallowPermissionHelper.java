package com.scansione.parselandroidtest.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.scansione.parselandroidtest.R;
import com.scansione.parselandroidtest.dialogs.Alerts;

import java.util.HashMap;
import java.util.Map;

public class MarshmallowPermissionHelper {

    /*Permission Checking for Location*/

    public static boolean getLocationPermission(final Fragment fragment, final Activity activity, final int REQUEST_CODE) {

        Context context = null;
        if (fragment != null)
            context = fragment.getContext();
        else
            context = activity;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.ACCESS_COARSE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.ACCESS_FINE_LOCATION)) {

                    android.support.v7.app.AlertDialog.Builder builder =
                            new android.support.v7.app.AlertDialog.Builder(activity);
                    builder.setTitle("Permission Required");
                    builder.setMessage("Location Permission Required. " +
                            "You have to grant this permission in order to use this feature.");
                    builder.setPositiveButton("Got it",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    if (fragment != null)
                                        fragment.requestPermissions(
                                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                                REQUEST_CODE);

                                    else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        activity.requestPermissions(
                                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                                REQUEST_CODE);
                                    }

                                }
                            });
                    builder.show();


                } else {


                    if (fragment != null)
                        fragment.requestPermissions(
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_CODE);

                    else activity.requestPermissions(
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_CODE);


                }
                return false;
            } else
                return true;
        } else {
            return true;
        }

    }
    /* Alert user to enable GPS to get there locations */
    public static boolean checkDeviceLocationSetting(final Activity mActivity,
                                                     final Alerts.TwoButtonAlert callback) {
        if (null != mActivity) {
            if (!isLocationSwitchEnabled(mActivity)) {
                /**
                 * Ask user to enable location service, to get user current location.
                 */
                Map<String, String> map = new HashMap<>();
                map.put(Alerts.DialogKeys.TITLE, "Location Service Alert!");
                map.put(Alerts.DialogKeys.MESSAGE, "Location service is not enabled in your device." +
                        " Please enable location switch(GPS) to get your current location.");
                map.put(Alerts.DialogKeys.POSITIVE_BUTTON_TEXT, "Open location setting");
                map.put(Alerts.DialogKeys.NEGATIVE_BUTTON_TEXT, "Cancel");
                Alerts.twoButtonAlert(mActivity, map, callback);
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /* Check location hardware (Location) switch */

    public static boolean isLocationSwitchEnabled(final Context mContext) {
        LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        return gps_enabled && network_enabled;
    }
}