package com.scansione.parselandroidtest.helper;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;


public class AppUtils {


    /**
     * Check for email validation using android default
     * email validator.
     *
     * @param target
     * @return boolean
     */
    public static final boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /**
     * Check for internet connection and if not available
     * automatically show specific toast message as alert.
     *
     * @return
     */
    public static final boolean isConnectingToInternet(Context _ctx) {
        if (null != _ctx) {
            ConnectivityManager connectivity = (ConnectivityManager) _ctx.getSystemService(Activity.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }

            }
        }
        return false;
    }


}
