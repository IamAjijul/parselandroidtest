package com.scansione.parselandroidtest.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.scansione.parselandroidtest.interfaces.PaperdbKeys;

import io.paperdb.Paper;


public class MyInstanceIDListenerService extends FirebaseInstanceIdService {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is also called
     * when the InstanceID token is initially generated, so this is where
     * you retrieve the token.
     */
    // [START refresh_token]
    String TAG = "MyInstanceIDListenerService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String refreshedToken) {
       /*We can write code to save token to server here*/
        Paper.book().write(PaperdbKeys.FCM_TOKEN, refreshedToken);
        Log.d("MyInstanceIDService", "-----FCM token-----" + refreshedToken);
    }


}

