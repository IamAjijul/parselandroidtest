package com.scansione.parselandroidtest.fcm;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.scansione.parselandroidtest.R;

import java.util.Map;


public class MyFcmMessageListenerService extends com.google.firebase.messaging.FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        handelWithNotificationData(remoteMessage);
    }

    void handelWithNotificationData(RemoteMessage remoteMessage) {
        Log.d("OnMessageReceived", "********" + remoteMessage.getNotification().getBody());
        showNotification(remoteMessage.getNotification().getBody());
    }

    private void showNotification(String data) {
      android.support.v4.app.NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mBuilder.setColor(getResources().getColor(R.color.colorPrimary, null));
        } else {
            mBuilder.setColor(getResources().getColor(R.color.colorPrimary));
        }
        mBuilder.setSmallIcon(R.drawable.logo)
                .setAutoCancel(true)
                .setContentTitle("Parsel Received")
                .setContentText(data);


        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(null);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(102, mBuilder.build());
    }


}

