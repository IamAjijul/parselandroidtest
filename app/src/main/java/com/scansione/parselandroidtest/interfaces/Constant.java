package com.scansione.parselandroidtest.interfaces;


public interface Constant {
    int LOCATION_REQUEST_CODE = 102;

    int LOCATION_SETTING_REQUEST_CODE = 103;
    String LONGITUDE = "long";
    String LATITUDE = "lat";
    String LOCATION_RECEIVER_ACTION = "com.scansione.parselandroidtest.ON_LOCATION_CHANGE";
}
