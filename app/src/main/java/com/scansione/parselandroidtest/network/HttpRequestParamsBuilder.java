package com.scansione.parselandroidtest.network;

import org.json.JSONObject;

import java.util.Map;


public class HttpRequestParamsBuilder {

    private Map headerParam;
    private String requestURL;
    private JSONObject jsonObject;

    public Map getHeaderParam() {
        return headerParam;
    }

    public void setHeaderParam(Map headerParam) {
        this.headerParam = headerParam;
    }

    public String getRequestURL() {
        return requestURL;
    }

    public void setRequestURL(String requestURL) {
        this.requestURL = requestURL;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}
