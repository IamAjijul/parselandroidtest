package com.scansione.parselandroidtest.network;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.Map;

public class RequestClient {

    private ProgressDialog dialog;
    Context context;
    private String TAG = "RequestClient";

    // Tag used to cancel the request
    public RequestClient(Context mContext) {
        this.context = mContext;
        dialog = new ProgressDialog(context);
    }

    /**
     * Show Network call progress dialog
     *
     * @param mContext
     * @param msg
     */
    private void showDialog(Context mContext, String msg) {
        dialog.setCancelable(false);
        dialog.setMessage(msg);
        dialog.show();
    }

    /**
     * Dismiss Progress Dialog
     */
    private void dismissDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    //Login
    public void callLoginApi(Context context, String email, String password, String device_info,
                             String device_token, String uuid,
                             String loaderMessage, BaseResponseListener mBaseResponseListener) {
        showDialog(context, loaderMessage);
        post(context,RequestParamBuilder.getLoginAllUrlParams(email, password, device_info, device_token, uuid),
                mBaseResponseListener);
    }


    private void post(Context context, final HttpRequestParamsBuilder mHttpRequestBuilder,
                      final BaseResponseListener mBaseResponseListener) {
        CustomJSONObjectRequest request = new CustomJSONObjectRequest(Request.Method.POST,
                mHttpRequestBuilder.getRequestURL(), mHttpRequestBuilder.getJsonObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mBaseResponseListener.requestCompleted(response.toString());
                        dismissDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mBaseResponseListener.requestEndedWithError(error);
                dismissDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return mHttpRequestBuilder.getHeaderParam();
            }

        };


        CustomVolleyRequestQueue.getInstance(context).addToRequestQueue(request);
    }


}
