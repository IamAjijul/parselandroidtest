package com.scansione.parselandroidtest.network;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class RequestParamBuilder {

    // Login


    public static HttpRequestParamsBuilder getLoginAllUrlParams(String email, String password,
                                                                String device_info, String device_token, String uuid) {

        HttpRequestParamsBuilder mHttpRequestBuilder = new HttpRequestParamsBuilder();

        try {
            mHttpRequestBuilder.setRequestURL(Urls.URL_LOGIN);
            mHttpRequestBuilder.setHeaderParam(getLoginHeadersParams(device_info, device_token, uuid));
            mHttpRequestBuilder.setJsonObject(getLoginJSONParams(email, password));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mHttpRequestBuilder;

    }

    public static Map<String, String> getLoginHeadersParams(String device_info, String device_token, String uuid) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("device_info", device_info);
        params.put("device_token", device_token);
        params.put("uuid", uuid);
        return params;
    }

    public static JSONObject getLoginJSONParams(String email, String password) {
        JSONObject params = new JSONObject();

        try {
            params.put("username", email);
            params.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return params;
    }
    // Login


}


