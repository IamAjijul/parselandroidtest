package com.scansione.parselandroidtest.network;

import com.android.volley.VolleyError;


public interface BaseResponseListener {
     void requestStarted();

     void requestCompleted(String response);

     void requestEndedWithError(VolleyError error);
}