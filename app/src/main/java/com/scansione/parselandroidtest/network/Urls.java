package com.scansione.parselandroidtest.network;


public interface Urls {

    String COMMON_TAG = "com.scansione.parselandroidtest";

    String BASE_URL = "https://pvr.parsel.in/pbb/sec/";

    String URL_LOGIN = BASE_URL + "login";

}
