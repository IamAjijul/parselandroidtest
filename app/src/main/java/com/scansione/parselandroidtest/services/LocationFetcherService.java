package com.scansione.parselandroidtest.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.scansione.parselandroidtest.interfaces.Constant;


public class LocationFetcherService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    GoogleApiClient myGoogleApiClient;
    LocationRequest mLocationRequest;
    final long LOCATION_REQUEST_NORMAL_INTERVAL = 10000;
    final long LOCATION_REQUEST_FASTEST_INTERVAL = 5000;
    private Intent intent = null;


    @Override
    public void onCreate() {
        intent = new Intent(Constant.LOCATION_RECEIVER_ACTION);
        myGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (myGoogleApiClient != null)
            myGoogleApiClient.connect();

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.e("LocationUpdate", "ON-DESTROY");
        if (myGoogleApiClient != null)
            myGoogleApiClient.disconnect();
        super.onDestroy();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("LocationUpdate", "ON-CONNECTED");
        LocationServices.FusedLocationApi.requestLocationUpdates(
                myGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.e("LocationUpdate", "ON-SUSPENDED");
        if (myGoogleApiClient != null)
            myGoogleApiClient.connect();

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("LocationUpdate", "ON-LOCATION-CHANGED Lat : " + location.getLatitude()
                + " LONG : " + location.getLongitude());
        updateDriverLocation(location);

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("LocationUpdate", "ON-FAILED");
        if (myGoogleApiClient != null)
            myGoogleApiClient.connect();

    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(LOCATION_REQUEST_NORMAL_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_REQUEST_FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

    }

    private void updateDriverLocation(Location location) {

        Log.d("Network", "Location" + location.getLatitude());
        intent.putExtra(Constant.LATITUDE, location.getLatitude());
        intent.putExtra(Constant.LONGITUDE, location.getLongitude());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


}
