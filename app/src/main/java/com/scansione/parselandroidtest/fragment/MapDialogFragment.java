package com.scansione.parselandroidtest.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.scansione.parselandroidtest.R;
import com.scansione.parselandroidtest.helper.GetDirectionOnMap;
import com.scansione.parselandroidtest.helper.LocationReceiver;
import com.scansione.parselandroidtest.interfaces.Constant;

import java.util.Locale;

import io.paperdb.Paper;


public class MapDialogFragment extends DialogFragment
        implements OnMapReadyCallback, LocationReceiver.LocationListener {

    private SupportMapFragment fragment;

    private View view;
    private LocationReceiver locationReceiver;
    private static boolean isMapReady = false;
    private static GoogleMap googleMap;
    private LatLng currentLocationLat;
    private Runnable runnable2;
    private boolean killMe = false;
    private Handler handlerMoving = new Handler();
    private Marker marker1 = null, marker2 = null;
    private LinearLayout progressLayout;


    public MapDialogFragment() {
        fragment = new SupportMapFragment();
        fragment.getMapAsync(this);


    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FixedHeightDialog);
        this.locationReceiver = new LocationReceiver(this);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(locationReceiver,
                new IntentFilter(Constant.LOCATION_RECEIVER_ACTION));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        progressLayout.setVisibility(View.VISIBLE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_map_select, container, false);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.mapSelect_customFlMap, fragment).commit();
        progressLayout = (LinearLayout) view.findViewById(R.id.progressLayout);
        return view;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.FixedHeightDialog;
        return dialog;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("MapDialog", "OnMapReady");
        isMapReady = true;
        this.googleMap = googleMap;
    }

    private void doWorkWithMap(final GoogleMap googleMap, Double lat, Double lng) {
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        currentLocationLat = new LatLng(lat, lng);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocationLat, 30));
        if (marker1 == null)
            marker1 = googleMap.addMarker(new MarkerOptions().position(currentLocationLat)
                    .title("Start Location").icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("map_icon", 70, 82))));
        if (marker2 == null)
            marker2 = googleMap.addMarker(new MarkerOptions().position(currentLocationLat)
                    .title("Current Location").icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("map_icon", 70, 82))).
                            flat(true));
        moveRoadRunner(marker2, currentLocationLat, false);
    }

    /*
     For Resize Custom Map Marker
    */
    public Bitmap resizeMapIcons(String iconName, int width, int height) {

        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),
                getResources().getIdentifier(iconName, "drawable", getContext().getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        killMe = true;
        if (handlerMoving != null && runnable2 != null)
            handlerMoving.removeCallbacks(runnable2);
        super.onDestroy();
    }


    @Override
    public void onLicationChange(Double lat, Double lng2) {
        progressLayout.setVisibility(View.GONE);

        if (isMapReady) {
            doWorkWithMap(googleMap, lat, lng2);
        }
    }

    public void moveRoadRunner(final Marker marker, final LatLng toPosition,
                               final boolean hideMarker) {
        final long start = SystemClock.uptimeMillis();
        final LatLng startPoint = marker.getPosition();
        final long duration = 1000;

        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        if (runnable2 == null) {
            runnable2 = new Runnable() {
                @Override
                public void run() {
                    if (killMe)
                        return;
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed
                            / duration);

                    LatLng currentPosition = new LatLng(
                            startPoint.latitude * (1 - t) + toPosition.latitude * t,
                            startPoint.longitude * (1 - t) + toPosition.longitude * t);
                    marker.setPosition(currentPosition);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPosition,
                            30));
                    Log.d("GetLocation", "WithIn Runnable 2*******************" + currentPosition);

                }
            };
        }
        handlerMoving.post(runnable2);
    }

}
