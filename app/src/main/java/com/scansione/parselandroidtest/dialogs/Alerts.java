package com.scansione.parselandroidtest.dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.scansione.parselandroidtest.R;

import java.util.Map;

public class Alerts {

    public static void twoButtonAlert(Activity mActivity, Map<String, String> map,
                                      final TwoButtonAlert twoButtonAlertCallback) {
        AlertDialog alertDialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.MyAlertDialogStyle);
        builder.setTitle(map.get(DialogKeys.TITLE));
        builder.setMessage(map.get(DialogKeys.MESSAGE));
        builder.setPositiveButton(map.get(DialogKeys.POSITIVE_BUTTON_TEXT), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (null != twoButtonAlertCallback) {
                    twoButtonAlertCallback.onPositiveButtonClick(dialogInterface);
                }
            }
        });
        builder.setNegativeButton(map.get(DialogKeys.NEGATIVE_BUTTON_TEXT), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (null != twoButtonAlertCallback) {
                    twoButtonAlertCallback.onNegativeButtonClick(dialogInterface);
                }
            }
        });
        alertDialog = builder.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
    }

   public interface TwoButtonAlert {
        void onPositiveButtonClick(DialogInterface dialogInterface);

        void onNegativeButtonClick(DialogInterface dialogInterface);
    }


    public interface DialogKeys {
        String TITLE = "title";
        String MESSAGE = "message";
        String POSITIVE_BUTTON_TEXT = "positive_button_text";
        String NEGATIVE_BUTTON_TEXT = "negative_button_text";
        String NATURAL_BUTTON_TEXT = "natural_button_text";
    }
}
